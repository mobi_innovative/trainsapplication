package mobi.trains.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import mobi.trains.R;
import mobi.trains.adapters.FavoritesListAdapter;
import mobi.trains.dialogs.StationPickerDialog;
import mobi.trains.models.FavoriteStationItem;
import mobi.trains.models.StationData;
import mobi.trains.models.StationTrainsResponse;
import mobi.trains.service.IStationTrains;
import mobi.trains.utils.FavoritesPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class StationsSelectorFragment extends BaseFragment implements StationPickerDialog.IStationPickListener {

    private StationPickerDialog stationPickerDialog;
    private RecyclerView rvFavoriteItems;

    List<FavoriteStationItem> favoriteStationItems = new ArrayList<>();
    FavoritesListAdapter favoritesListAdapter;
    FavoritesPreferences favoritesPreferences;

    public StationsSelectorFragment() {
        favoritesListAdapter = new FavoritesListAdapter(favoriteStationItems);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.fragment_favs_list, container, false);
        rvFavoriteItems = inflatedView.findViewById(R.id.rv_favorite_items);
        favoritesPreferences = new FavoritesPreferences(getContext());
        return inflatedView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvFavoriteItems.setAdapter(favoritesListAdapter);
        rvFavoriteItems.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    void showStationPickerDialog() {
        if (stationPickerDialog == null && getContext() != null) {
            stationPickerDialog = new StationPickerDialog(getContext(), this);
        }
        stationPickerDialog.show();
    }

}
