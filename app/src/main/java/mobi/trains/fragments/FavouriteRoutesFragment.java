package mobi.trains.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import mobi.trains.R;
import mobi.trains.customlisteners.OnFabClickListener;
import mobi.trains.models.FavoriteStationItem;
import mobi.trains.models.StationData;
import mobi.trains.models.StationTrainsResponse;
import mobi.trains.service.IStationTrains;
import mobi.trains.utils.CollectionUtils;
import mobi.trains.utils.FavoritesPreferences;
import mobi.trains.utils.StationsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteRoutesFragment extends StationsSelectorFragment implements OnFabClickListener {

    private static final String SPLIT_ROUTE_STATIONS = " - ";
    private String startStation;

    public FavouriteRoutesFragment() {
        // Required empty public constructor
        super();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        populateSavedRoutes();
    }

    @Override
    public void onFabClicked() {
        showStationPickerDialog();
    }

    @Override
    public void onStationChosen(String stationName) {
        if (TextUtils.isEmpty(startStation)) {
            startStation = stationName;
            reopenDialog();
        } else {
            saveFavoriteRoute(startStation, stationName);
            processRouteFromStations(startStation, stationName);
            startStation = "";
        }
    }

    private void reopenDialog() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showStationPickerDialog();
            }
        }, 300);
    }

    private void populateSavedRoutes() {
        if (favoritesPreferences != null) {
            List<String> favoriteRoutes = favoritesPreferences.getFavoriteRoutes();
            if (CollectionUtils.collectionHasItems(favoriteRoutes)) {
                for (String item : favoriteRoutes) {
                    String[] split = item.split(FavoritesPreferences.FAV_ROUTE_SEPARATOR);
                    if (split.length > 1) {
                        String firstStation = split[0];
                        String endStation = split[1];
                        processRouteFromStations(firstStation, endStation);
                    }
                }
            }
        }
    }

    private void processRouteFromStations(String startStation, String endStation) {
        String routeName = formatRouteName(startStation, endStation);
        addRouteToList(routeName);
        fetchStationTrains(routeName, startStation, endStation);
    }

    private String formatRouteName(String firstStation, String endStation) {
        return firstStation.concat(SPLIT_ROUTE_STATIONS).concat(endStation);
    }

    private void saveFavoriteRoute(String startStation, String endStation) {
        favoritesPreferences.addFavRoute(startStation, endStation);
    }

    private void addRouteToList(String stationName) {
        favoriteStationItems.add(new FavoriteStationItem(stationName));
    }

    private void fetchStationTrains(final String stationName, final String firstStation, final String endStation) {

        IStationTrains stationTrains = trainApplication.getStationTrains();
        if (stationTrains != null) {
            Call<StationTrainsResponse> call = stationTrains.getStationTrains(firstStation);
            call.enqueue(new Callback<StationTrainsResponse>() {
                @Override
                public void onResponse(Call<StationTrainsResponse> call, Response<StationTrainsResponse> response) {
                    StationTrainsResponse body = response.body();
                    if (body != null) {
                        List<StationData> stationDataList = body.getStationDataList();
                        for (FavoriteStationItem item : favoriteStationItems) {
                            if (stationName.equals(item.getStationName())) {
                                List<StationData> filteredList = filterTrainsByDestination(stationDataList, firstStation, endStation);
                                item.setStationDataList(filteredList);
                                break;
                            }
                        }
                        favoritesListAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<StationTrainsResponse> call, Throwable t) {
                    Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private List<StationData> filterTrainsByDestination(List<StationData> stationDataList, String firstStation, String endStation) {

        List<StationData> filteredDataList = new ArrayList<>();
        if (!CollectionUtils.collectionHasItems(stationDataList)) {
            return filteredDataList;
        }

        int firstStationPosition = StationsManager.getInstance().findIndexOfStation(firstStation);
        int endStationPosition = StationsManager.getInstance().findIndexOfStation(endStation);
        boolean isGoingToTheEnd = endStationPosition > firstStationPosition;

        for (StationData item : stationDataList) {
            String destination = item.getDestination();
            int trainDestinationPosition = StationsManager.getInstance().findIndexOfStation(destination);
            if (isGoingToTheEnd && trainDestinationPosition > endStationPosition) {
                filteredDataList.add(item);
            } else if (!isGoingToTheEnd && trainDestinationPosition < endStationPosition) {
                filteredDataList.add(item);
            }
        }
        return filteredDataList;
    }
}
