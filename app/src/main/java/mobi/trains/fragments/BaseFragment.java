package mobi.trains.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mobi.trains.application.ITrainApplication;

public class BaseFragment extends Fragment {

    protected ITrainApplication trainApplication;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            trainApplication = (ITrainApplication) getActivity().getApplication();
        }
    }
}
