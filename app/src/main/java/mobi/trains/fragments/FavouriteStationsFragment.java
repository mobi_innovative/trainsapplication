package mobi.trains.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import mobi.trains.R;
import mobi.trains.customlisteners.OnFabClickListener;
import mobi.trains.models.FavoriteStationItem;
import mobi.trains.models.StationData;
import mobi.trains.models.StationTrainsResponse;
import mobi.trains.service.IStationTrains;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteStationsFragment extends StationsSelectorFragment implements OnFabClickListener {

    public FavouriteStationsFragment() {
        // Required empty public constructor
        super();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        populateSavedStations();
    }

    private void populateSavedStations() {
        if (favoritesPreferences != null) {
            List<String> favStations = favoritesPreferences.getFavoriteStations();
            for (String item : favStations) {
                addStationToList(item);
                fetchStationTrains(item);
            }
        }
    }

    @Override
    public void onFabClicked() {
        showStationPickerDialog();
    }

    @Override
    public void onStationChosen(final String stationName) {
        if (favoritesPreferences != null) {
            favoritesPreferences.addFavStation(stationName);
        }
        addStationToList(stationName);
        fetchStationTrains(stationName);
    }

    private void addStationToList(String stationName) {
        favoriteStationItems.add(new FavoriteStationItem(stationName));
    }

    private void fetchStationTrains(final String stationName) {
        IStationTrains stationTrains = trainApplication.getStationTrains();
        if (stationTrains != null) {
            Call<StationTrainsResponse> call = stationTrains.getStationTrains(stationName);
            call.enqueue(new Callback<StationTrainsResponse>() {
                @Override
                public void onResponse(Call<StationTrainsResponse> call, Response<StationTrainsResponse> response) {
                    StationTrainsResponse body = response.body();
                    if (body != null) {
                        List<StationData> stationDataList = body.getStationDataList();
                        for (FavoriteStationItem item : favoriteStationItems) {
                            if (stationName.equals(item.getStationName())) {
                                item.setStationDataList(stationDataList);
                                break;
                            }
                        }
                        favoritesListAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<StationTrainsResponse> call, Throwable t) {
                    Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

}
