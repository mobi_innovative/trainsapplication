package mobi.trains.utils;

import java.util.List;

import mobi.trains.models.Station;

public class StationsManager {

    private static final int STATION_NOT_FOUND = -1;

    private static StationsManager instance;

    private List<Station> stationArrayList;

    public static StationsManager getInstance() {
        if (instance == null) {
            instance = new StationsManager();
        }
        return instance;
    }

    public List<Station> getStationArrayList() {
        return stationArrayList;
    }

    public void setStationArrayList(List<Station> stationArrayList) {
        this.stationArrayList = stationArrayList;
    }

    public int findIndexOfStation(String name) {
        if (CollectionUtils.collectionHasItems(stationArrayList)) {
            for (int i = 0; i < stationArrayList.size(); i++) {
                final Station station = stationArrayList.get(i);
                if (name.equals(station.getName())) {
                    return i;
                }
            }
        }
        return STATION_NOT_FOUND;
    }
}
