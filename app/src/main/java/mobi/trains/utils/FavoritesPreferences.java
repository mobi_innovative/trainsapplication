package mobi.trains.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class FavoritesPreferences {

    private static final String TRAIN_PREFS = "train_prefs";
    private static final String FAV_STATIONS_KEY = "fav_stations_key";
    private static final String FAV_ROUTES_KEY = "fav_routes_key";
    private static final String FIRST_RUN_KEY = "first_run_key";
    private static final String STATIONS_SEPARATOR = ";";

    public static final String FAV_ROUTE_SEPARATOR = "=";

    private SharedPreferences preferences;

    public FavoritesPreferences(Context context) {
        if (context != null) {
            this.preferences = context.getSharedPreferences(TRAIN_PREFS, MODE_PRIVATE);
        }
    }

    public void setFirstRunPassed() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(FIRST_RUN_KEY, false);
        editor.apply();
    }

    public boolean isFirstRun() {
        return preferences.getBoolean(FIRST_RUN_KEY, true);
    }

    public void addFavStation(String stationName) {
        if (preferences != null) {
            String favsString = preferences.getString(FAV_STATIONS_KEY, "");
            String newFavsString = concatNewStation(favsString, stationName);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(FAV_STATIONS_KEY, newFavsString);
            editor.apply();
        }
    }

    private String concatNewStation(String favsString, String newStation) {
        if (TextUtils.isEmpty(favsString)) {
            return favsString.concat(newStation);
        } else {
            return favsString.concat(STATIONS_SEPARATOR).concat(newStation);
        }
    }

    public void addFavRoute(String startStation, String endStation) {
        if (preferences != null) {
            String favRoutes = preferences.getString(FAV_ROUTES_KEY, "");
            String newFavsRoute = concatNewRoute(favRoutes, startStation, endStation);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(FAV_ROUTES_KEY, newFavsRoute);
            editor.apply();
        }
    }

    private String concatNewRoute(String favsString, String startStation, String endStation) {
        String newRoute = startStation.concat(FAV_ROUTE_SEPARATOR).concat(endStation);
        if (TextUtils.isEmpty(favsString)) {
            return newRoute;
        } else {
            return favsString.concat(STATIONS_SEPARATOR).concat(newRoute);
        }
    }

    private List<String> getFavoritesList(String key) {
        if (preferences != null) {
            String favsStationsString = preferences.getString(key, "");
            if (TextUtils.isEmpty(favsStationsString)) {
                return new ArrayList<>();
            } else {
                return Arrays.asList(favsStationsString.split(";"));
            }
        }
        return new ArrayList<>();
    }

    public List<String> getFavoriteStations() {
        return getFavoritesList(FAV_STATIONS_KEY);
    }

    public List<String> getFavoriteRoutes() {
        return getFavoritesList(FAV_ROUTES_KEY);
    }


}
