package mobi.trains.utils;

import java.util.Collection;

public class CollectionUtils {

    public static boolean collectionHasItems(Collection collection) {
        return collection != null && collection.size() > 0;
    }

}
