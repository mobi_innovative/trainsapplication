package mobi.trains.service;

import mobi.trains.models.StationsListResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface IStationsList {

    @GET(NetworkConstants.ALL_STATIONS_URL)
    Call<StationsListResponse> getStations();
}
