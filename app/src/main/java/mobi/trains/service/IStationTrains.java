package mobi.trains.service;

import mobi.trains.models.StationTrainsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IStationTrains {

    @GET(NetworkConstants.STATION_TRAINS_URL)
    Call<StationTrainsResponse> getStationTrains(@Query("StationDesc") String stationName);
}
