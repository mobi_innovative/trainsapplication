package mobi.trains.service;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class RetrofitModule {

    public Retrofit getRetrofitClient() {
        return new Retrofit.Builder()
                .baseUrl(NetworkConstants.API_BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

    private OkHttpClient getOkHttpClient() {
        HttpLoggingInterceptor interceptor = getHttpLoggingInterceptor();
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    private HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

}
