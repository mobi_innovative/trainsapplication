package mobi.trains.service;

public class NetworkConstants {

    public static final String API_BASE_URL = "http://api.irishrail.ie/realtime/realtime.asmx/";
    public static final String ALL_STATIONS_URL = "getAllStationsXML";
    public static final String STATION_TRAINS_URL = "getStationDataByNameXML";

}
