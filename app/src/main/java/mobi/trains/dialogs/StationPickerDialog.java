package mobi.trains.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import mobi.trains.R;
import mobi.trains.adapters.StationsListAdapter;

public class StationPickerDialog extends Dialog implements StationsListAdapter.OnItemClickListener {

    private IStationPickListener stationPickListener;
    private StationsListAdapter stationsListAdapter;

    public StationPickerDialog(@NonNull Context context, IStationPickListener stationPickListener) {
        super(context);
        this.stationPickListener = stationPickListener;
        this.stationsListAdapter = new StationsListAdapter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_station);
        RecyclerView recyclerView = findViewById(R.id.rv_stations_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(stationsListAdapter);
    }

    @Override
    public void onItemClicked(String itemName) {
        if (stationPickListener != null) {
            stationPickListener.onStationChosen(itemName);
            dismiss();
        }
    }

    public interface IStationPickListener {
        void onStationChosen(String stationName);
    }

}
