package mobi.trains.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import mobi.trains.application.ITrainApplication;
import mobi.trains.utils.ConnectionUtil;

public abstract class BaseActivity extends AppCompatActivity {

    protected ITrainApplication trainApplication;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trainApplication = (ITrainApplication) getApplicationContext();
    }

    @Override
    protected void onResume() {

        ConnectionUtil.checkNetworkConnection(this);
        super.onResume();
    }
}
