package mobi.trains.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.List;

import mobi.trains.R;
import mobi.trains.adapters.FragmentsPagerAdapter;
import mobi.trains.customlisteners.OnFabClickListener;
import mobi.trains.utils.CollectionUtils;

public class MainActivity extends BaseActivity {

    private ViewPager viewPager;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initListeners();
        setupViewPager();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            moveTaskToBack(true);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.press_again_to_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void setupViewPager() {
        FragmentsPagerAdapter fragmentsPagerAdapter = new FragmentsPagerAdapter(getSupportFragmentManager(), this);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(fragmentsPagerAdapter);
        ((TabLayout) findViewById(R.id.tab_layout)).setupWithViewPager(viewPager);
    }

    private void initListeners() {
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Fragment> fragments = getSupportFragmentManager().getFragments();
                int currentItem = viewPager.getCurrentItem();
                boolean hasEnoughInitializedFragments = CollectionUtils.collectionHasItems(fragments)
                        && currentItem < fragments.size();

                if (hasEnoughInitializedFragments) {
                    Fragment fragment = fragments.get(currentItem);
                    if (fragment instanceof OnFabClickListener) {
                        ((OnFabClickListener) fragment).onFabClicked();
                    }
                }
            }
        });
    }

}
