package mobi.trains.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.List;

import mobi.trains.R;
import mobi.trains.models.Station;
import mobi.trains.models.StationsListResponse;
import mobi.trains.service.IStationsList;
import mobi.trains.utils.StationsManager;
import mobi.trains.utils.CollectionUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity implements Callback<StationsListResponse> {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_splash);
        super.onCreate(savedInstanceState);

        IStationsList stationsList = trainApplication.getStationsList();
        if (stationsList != null) {
            Call<StationsListResponse> stations = stationsList.getStations();
            stations.enqueue(this);
        }
    }

    @Override
    public void onResponse(Call<StationsListResponse> call, Response<StationsListResponse> response) {
        StationsListResponse body = response.body();
        if (body != null) {
            boolean shouldOpenMainScreen = processResponse(body);
            if (shouldOpenMainScreen) navigateToMain();
        }
    }

    private void navigateToMain() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private boolean processResponse(StationsListResponse body) {
        List<Station> stationsList = body.getStationsList();
        if (CollectionUtils.collectionHasItems(stationsList)) {
            StationsManager.getInstance().setStationArrayList(stationsList);
            return true;
        } else {
            Toast.makeText(this, getString(R.string.stations_call_failed), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onFailure(Call<StationsListResponse> call, Throwable t) {
        Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
    }
}
