package mobi.trains.customlisteners;

public interface OnFabClickListener {
    void onFabClicked();
}
