package mobi.trains.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mobi.trains.R;
import mobi.trains.models.FavoriteStationItem;
import mobi.trains.models.StationData;
import mobi.trains.utils.CollectionUtils;

public class FavoritesListAdapter extends RecyclerView.Adapter<FavoritesListAdapter.ItemViewHolder> {

    private List<FavoriteStationItem> favoriteStationItems;

    public FavoritesListAdapter(List<FavoriteStationItem> favoriteStationItems) {
        this.favoriteStationItems = favoriteStationItems;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_item, parent, false);
        return new ItemViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final FavoriteStationItem favoriteStationItem = favoriteStationItems.get(position);
        holder.tvStationName.setText(favoriteStationItem.getStationName());
        holder.llTrainsHolder.removeAllViews();
        List<StationData> stationDataList = favoriteStationItem.getStationDataList();
        if (CollectionUtils.collectionHasItems(stationDataList)) {
            for (StationData item : stationDataList) {
                View view = createTrainItemView(item, holder.itemView);
                holder.llTrainsHolder.addView(view);
            }
        } else {
            TextView textView = new TextView(holder.itemView.getContext());
            textView.setText(holder.itemView.getContext().getString(R.string.no_trains_running));
            holder.llTrainsHolder.addView(textView);
        }
    }

    private View createTrainItemView(StationData item, View parentView) {
        View inflatedView = LayoutInflater.from(parentView.getContext()).inflate(R.layout.train_item, null);

        //remark - those values are Ireland times - (GMT+1), which I've left like this
        //because a person who travels every day is supposed to live in Ireland :D (and TBH I was a bit lazy to make convertion
        //to Android system locale :D :D :D
        ((TextView) inflatedView.findViewById(R.id.tv_train_code)).setText(item.getTrainCode());
        ((TextView) inflatedView.findViewById(R.id.tv_origin)).setText(parentView.getContext().getString(R.string.origin_from).concat(item.getOrigin()));
        ((TextView) inflatedView.findViewById(R.id.tv_destination)).setText(parentView.getContext().getString(R.string.destination_to).concat(item.getDestination()));
        ((TextView) inflatedView.findViewById(R.id.tv_arrival_time)).setText(item.getExpectedArrival());
        ((TextView) inflatedView.findViewById(R.id.tv_departure_time)).setText(item.getExpectedDeparture());

        return inflatedView;
    }

    @Override
    public int getItemCount() {
        return favoriteStationItems != null ? favoriteStationItems.size() : 0;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvStationName;
        LinearLayout llTrainsHolder;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvStationName = itemView.findViewById(R.id.tv_name);
            llTrainsHolder = itemView.findViewById(R.id.ll_trains_holder);
        }
    }

}
