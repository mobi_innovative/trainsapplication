package mobi.trains.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mobi.trains.R;
import mobi.trains.models.Station;
import mobi.trains.utils.StationsManager;

public class StationsListAdapter extends RecyclerView.Adapter<StationsListAdapter.StationHolder> {

    private List<Station> stationList;
    private OnItemClickListener onItemClickListener;

    public StationsListAdapter(OnItemClickListener onItemClickListener) {
        this.stationList = StationsManager.getInstance().getStationArrayList();
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public StationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.station_item, parent, false);
        return new StationHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull StationHolder holder, int position) {
        final String stationName = stationList.get(position).getName();
        holder.tvStationName.setText(stationName);
    }

    @Override
    public int getItemCount() {
        return stationList != null ? stationList.size() : 0;
    }

    class StationHolder extends RecyclerView.ViewHolder {

        TextView tvStationName;

        StationHolder(@NonNull View itemView) {
            super(itemView);
            tvStationName = itemView.findViewById(R.id.tv_station_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClicked(stationList.get(getAdapterPosition()).getName());
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(String itemName);
    }
}
