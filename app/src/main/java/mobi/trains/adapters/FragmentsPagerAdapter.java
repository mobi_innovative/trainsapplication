package mobi.trains.adapters;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import mobi.trains.R;
import mobi.trains.fragments.FavouriteRoutesFragment;
import mobi.trains.fragments.FavouriteStationsFragment;

public class FragmentsPagerAdapter extends FragmentPagerAdapter {

    private static final int NUMBER_OF_TABS = 2;
    private Context context;

    public FragmentsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FavouriteRoutesFragment();
        } else {
            return new FavouriteStationsFragment();
        }
    }

    @Override
    public int getCount() {
        return NUMBER_OF_TABS;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context != null ? context.getString(R.string.fav_routes_title) : "";
            case 1:
                return context != null ? context.getString(R.string.fav_stations_title) : "";
            default:
                return "";
        }
    }

}
