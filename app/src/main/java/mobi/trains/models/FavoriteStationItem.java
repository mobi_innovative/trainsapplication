package mobi.trains.models;

import java.util.List;

public class FavoriteStationItem {

    private String stationName;
    private List<StationData> stationDataList;

    public FavoriteStationItem(String stationName) {
        this.stationName = stationName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public List<StationData> getStationDataList() {
        return stationDataList;
    }

    public void setStationDataList(List<StationData> stationDataList) {
        this.stationDataList = stationDataList;
    }

}
