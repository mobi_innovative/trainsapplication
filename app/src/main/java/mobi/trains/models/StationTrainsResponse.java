package mobi.trains.models;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ArrayOfObjStation", strict = false)
public class StationTrainsResponse {

    @ElementList(name = "ArrayOfObjStation", inline = true, required = false)
    private List<StationData> stationDataList;

    public List<StationData> getStationDataList() {
        return stationDataList;
    }

    public void setStationDataList(List<StationData> stationDataList) {
        this.stationDataList = stationDataList;
    }
}
