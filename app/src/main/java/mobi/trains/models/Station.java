package mobi.trains.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "objStation", strict = false)
public class Station {

    @Element(name = "StationDesc")
    private String name;

    @Element(name = "StationId")
    private String stationId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

}
