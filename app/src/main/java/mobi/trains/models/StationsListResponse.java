package mobi.trains.models;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ArrayOfObjStation", strict = false)
public class StationsListResponse {

    @ElementList(name = "ArrayOfObjStation", inline = true)
    private List<Station> stationsList;

    public List<Station> getStationsList() {
        return stationsList;
    }

    public void setStationsList(List<Station> stationsList) {
        this.stationsList = stationsList;
    }
}
