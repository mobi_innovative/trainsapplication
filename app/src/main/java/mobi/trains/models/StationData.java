package mobi.trains.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "objStationData", strict = false)
public class StationData {

    @Element(name = "Origin")
    private String origin;

    @Element(name = "Destination")
    private String destination;

    @Element(name = "Exparrival")
    private String expectedArrival;

    @Element(name = "Expdepart")
    private String expectedDeparture;

    @Element(name = "Traincode")
    private String trainCode;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getExpectedArrival() {
        return expectedArrival;
    }

    public void setExpectedArrival(String expectedArrival) {
        this.expectedArrival = expectedArrival;
    }

    public String getExpectedDeparture() {
        return expectedDeparture;
    }

    public void setExpectedDeparture(String expectedDeparture) {
        this.expectedDeparture = expectedDeparture;
    }

    public String getTrainCode() {
        return trainCode;
    }

    public void setTrainCode(String trainCode) {
        this.trainCode = trainCode;
    }
}
