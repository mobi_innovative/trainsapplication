package mobi.trains.application;

import android.app.Application;
import android.util.Log;

import mobi.trains.service.IStationTrains;
import mobi.trains.service.IStationsList;
import mobi.trains.service.RetrofitModule;
import mobi.trains.utils.FavoritesPreferences;
import retrofit2.Retrofit;

public class TrainsApplication extends Application implements ITrainApplication {

    public static final String ARKLOW = "Arklow";
    public static final String SHANKILL = "Shankill";
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        // I am using SimpleXmlConverterFactory, although it is deprecated. Jake Wharton
        // advised on using // https://github.com/Tickaroo/tikxml, however for a short time
        // I decided to go for the deprecated class
        retrofit = new RetrofitModule().getRetrofitClient();

        FavoritesPreferences favoritesPreferences = new FavoritesPreferences(getApplicationContext());
        boolean firstRun = favoritesPreferences.isFirstRun();
        Log.e("NIKO", "FirstRun = " + firstRun);
        if (firstRun) {
            favoritesPreferences.addFavRoute(SHANKILL, ARKLOW);
            favoritesPreferences.addFavRoute(ARKLOW, SHANKILL);
            favoritesPreferences.addFavStation(SHANKILL);
            favoritesPreferences.addFavStation(ARKLOW);
            favoritesPreferences.setFirstRunPassed();
        }
    }

    @Override
    public IStationsList getStationsList() {
        if (retrofit != null) {
            return retrofit.create(IStationsList.class);
        }
        return null;
    }

    @Override
    public IStationTrains getStationTrains() {
        if (retrofit != null) {
            return retrofit.create(IStationTrains.class);
        }
        return null;
    }
}
