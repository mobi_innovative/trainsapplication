package mobi.trains.application;

import mobi.trains.service.IStationTrains;
import mobi.trains.service.IStationsList;

public interface ITrainApplication {

    IStationsList getStationsList();
    IStationTrains getStationTrains();

}
